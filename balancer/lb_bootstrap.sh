#!/bin/bash

echo "Generating configuration..."
export MASTER_IP=tcp://$(docker-machine ssh swarm-master 'ifconfig eth1 | grep "inet addr:" | cut -d: -f2 | cut -d" " -f1'):3376

echo $MASTER_IP

#docker-machine ssh balancer mkdir /interlock
#docker-machine scp config.toml balancer:/interlock

eval $(docker-machine env balancer)

docker-machine scp swarm-master:/etc/docker/* balancer:/etc/docker/

docker-compose up -d nginx
docker-compose up -d interlock


