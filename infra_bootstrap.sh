 #!/bin/bash
echo "Exporting keys...\n"

export DIGITALOCEAN_ACCESS_TOKEN=<your_digitalocean_private_token>
export DIGITALOCEAN_PRIVATE_NETWORKING=true
export DIGITALOCEAN_IMAGE=debian-8-x64
export DIGITALOCEAN_REGION=ams2

echo "creating consul host...\n"
docker-machine create   -d digitalocean --digitalocean-size 2gb consul
echo "creating metrics host...\n"
docker-machine create   -d digitalocean --digitalocean-size 2gb metrics

echo "starting consul service...\n"
export KV_IP=$(docker-machine ssh consul 'ifconfig eth1 | grep "inet addr:" | cut -d: -f2 | cut -d" " -f1')
eval $(docker-machine env consul)
docker run -d   -p ${KV_IP}:8500:8500   -h consul   --restart always   gliderlabs/consul-server -bootstrap

echo "creating swarm master...\n"
docker-machine create   -d digitalocean --digitalocean-size 2gb \
--swarm   --swarm-master \
--swarm-discovery="consul://${KV_IP}:8500" \
--engine-opt="cluster-store=consul://${KV_IP}:8500" \
--engine-opt="cluster-advertise=eth1:2376" swarm-master

export MASTER_IP=$(docker-machine ssh swarm-master 'ifconfig eth1 | grep "inet addr:" | cut -d: -f2 | cut -d" " -f1')

echo "creating balancer host...\n"
docker-machine create   -d digitalocean --digitalocean-size 2gb balancer

echo "creating node hosts...\n"
docker-machine create   -d digitalocean \
--swarm   --swarm-discovery="consul://${KV_IP}:8500" \
--engine-opt="cluster-store=consul://${KV_IP}:8500" \
--engine-opt="cluster-advertise=eth1:2376" node01
export NODE01_IP=$(docker-machine ssh node01 'ifconfig eth1 | grep "inet addr:" | cut -d: -f2 | cut -d" " -f1')



docker-machine create   -d digitalocean \
--swarm   --swarm-discovery="consul://${KV_IP}:8500" \
--engine-opt="cluster-store=consul://${KV_IP}:8500" \
--engine-opt="cluster-advertise=eth1:2376" node02
export NODE02_IP=$(docker-machine ssh node02 'ifconfig eth1 | grep "inet addr:" | cut -d: -f2 | cut -d" " -f1')

echo "bootstrapping metrics...\n"
cd metrics
eval $(docker-machine env metrics)
docker-compose up -d
echo "preparing telegraf..."
cd telegraf
docker-machine ssh swarm-master 'mkdir /telegraf'
docker-machine ssh node01 'mkdir /telegraf'
docker-machine ssh node02 'mkdir /telegraf'
docker-machine scp telegraf.conf swarm-master:/telegraf
docker-machine scp telegraf.conf node01:/telegraf
docker-machine scp telegraf.conf node02:/telegraf
eval $(docker-machine env swarm-master)
export METRICS_IP=$(docker-machine ssh metrics 'ifconfig eth1 | grep "inet addr:" | cut -d: -f2 | cut -d" " -f1')
export NODE_NAME=swarm-master
docker-compose up -d
eval $(docker-machine env node01)
export NODE_NAME=node01
docker-compose up -d
eval $(docker-machine env node02)
export NODE_NAME=node01
docker-compose up -d

echo "preparing registrator..."
eval $(docker-machine env swarm-master)
docker run -d \
    --name=registrator_master \
    --net=host \
    --volume=/var/run/docker.sock:/tmp/docker.sock \
    gliderlabs/registrator:latest \
    -ip $MASTER_IP \
      consul://$KV_IP:8500

eval $(docker-machine env node01)
docker run -d \
    --name=registrator_node01 \
    --net=host \
    --volume=/var/run/docker.sock:/tmp/docker.sock \
    gliderlabs/registrator:latest \
    -ip $NODE01_IP \
      consul://$KV_IP:8500

eval $(docker-machine env node02)
docker run -d \
    --name=registrator_node02 \
    --net=host \
    --volume=/var/run/docker.sock:/tmp/docker.sock \
    -ip $NODE02_IP \
    gliderlabs/registrator:latest \
      consul://$KV_IP:8500
echo "finished! now configure a load balancing system with consl-template"

